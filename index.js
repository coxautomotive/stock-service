var seneca = require('seneca')();

seneca.add({role: 'stock', cmd: 'getById'}, function(msg, respond) {
  var stock = {count: 17};
  respond(null, stock);
});

seneca.listen();

seneca.ready(function() {
  console.log('stock-service ready!');
});
